package com.phxism;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.phxism.domain.User;
import com.phxism.domain.Utils;

public class JDKSerializationTests {
	private static User user;

	@BeforeClass
	public static void init() {
		user = new User("phxism@163.com", "hao", "zhang");
		List<User> friends = new ArrayList<User>();
		User friendA = new User("a@qq.com", "a", "aa");
		User friendB = new User("b@qq.com", "b", "bb");
		friends.add(friendA);
		friends.add(friendB);
		user.setFriends(friends);
	}

	@Test
	public void test01() {
		long start = System.currentTimeMillis();
		byte[] data = Utils.serialize(user);
		User user = Utils.deserialize(data);
		long end = System.currentTimeMillis();
		System.out.println("1耗时:" + (end - start));
		System.out.println(user);
	}

}
