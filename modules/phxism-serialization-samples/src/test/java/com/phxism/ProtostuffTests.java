package com.phxism;

import io.protostuff.JsonIOUtil;
import io.protostuff.LinkedBuffer;
import io.protostuff.ProtobufIOUtil;
import io.protostuff.ProtostuffIOUtil;
import io.protostuff.runtime.RuntimeSchema;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.phxism.domain.User;

public class ProtostuffTests {

	private static User user;
	// this is lazily created and cached by RuntimeSchema
	// so its safe to call RuntimeSchema.getSchema(User.class) over and over
	// The getSchema method is also thread-safe
	// Schema<User> schema = RuntimeSchema.getSchema(User.class);
	// Generates a schema from the given class.
	RuntimeSchema<User> schema = RuntimeSchema.createFrom(User.class);

	@BeforeClass
	public static void init() {
		user = new User("phxism@163.com", "hao", "zhang");
		List<User> friends = new ArrayList<User>();
		User friendA = new User("a@qq.com", "a", "aa");
		User friendB = new User("b@qq.com", "b", "bb");
		friends.add(friendA);
		friends.add(friendB);
		user.setFriends(friends);
	}

	@Test
	public void test01ProtostuffIOUtil() {

		LinkedBuffer buffer = LinkedBuffer.allocate();
		/* -------- protostuff -------- (requires protostuff-core module) */
		long start = System.currentTimeMillis();
		byte[] protostuff;
		// ser
		try {
			protostuff = ProtostuffIOUtil.toByteArray(user, schema, buffer);
		} finally {
			buffer.clear();
		}
		// deser
		User user = schema.newMessage();
		ProtostuffIOUtil.mergeFrom(protostuff, user, schema);
		long end = System.currentTimeMillis();
		System.out.println("1耗时:" + (end - start));
		System.out.println(user);
	}

	@Test
	public void test02ProtobufIOUtil() {
		LinkedBuffer buffer = LinkedBuffer.allocate();
		/* -------- protobuf -------- (requires protostuff-core module) */
		long start = System.currentTimeMillis();
		// ser
		byte[] protobuf;
		try {
			protobuf = ProtobufIOUtil.toByteArray(user, schema, buffer);
		} finally {
			buffer.clear();
		}
		// deser
		User user = schema.newMessage();
		ProtobufIOUtil.mergeFrom(protobuf, user, schema);
		long end = System.currentTimeMillis();
		System.out.println("2耗时:" + (end - start));
		System.out.println(user);
	}

	@Test
	// Reading/Writing from/to streams
	public void test03ProtostuffIOUtil() throws IOException {
		LinkedBuffer buffer = LinkedBuffer.allocate();

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		/* -------- protostuff -------- (requires protostuff-core module) */
		long start = System.currentTimeMillis();
		// ser
		try {
			ProtostuffIOUtil.writeTo(outputStream, user, schema, buffer);
		} finally {
			buffer.clear();
		}
		InputStream inputStream = new ByteArrayInputStream(
				outputStream.toByteArray());
		// deser
		User user = schema.newMessage();
		ProtostuffIOUtil.mergeFrom(inputStream, user, schema, buffer);
		long end = System.currentTimeMillis();
		System.out.println("3耗时:" + (end - start));
		System.out.println(user);
	}

	@Test
	// Reading/Writing from/to streams
	public void test04ProtobufIOUtil() throws IOException {
		LinkedBuffer buffer = LinkedBuffer.allocate();

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		/* --------protobuf -------- (requires protostuff-core module) */
		long start = System.currentTimeMillis();
		// ser
		try {
			ProtobufIOUtil.writeTo(outputStream, user, schema, buffer);
		} finally {
			buffer.clear();
		}
		InputStream inputStream = new ByteArrayInputStream(
				outputStream.toByteArray());
		// deser
		User user = schema.newMessage();
		ProtobufIOUtil.mergeFrom(inputStream, user, schema, buffer);
		long end = System.currentTimeMillis();
		System.out.println("4耗时:" + (end - start));
		System.out.println(user);
	}

	@Test
	public void test05JsonIOUtil() throws IOException {
		LinkedBuffer buffer = LinkedBuffer.allocate();
		/* -------- json -------- (requires protostuff-json module) */
		long start = System.currentTimeMillis();
		// ser
		boolean numeric = true;
		byte[] json;
		try {
			json = JsonIOUtil.toByteArray(user, schema, numeric, buffer);
		} finally {
			buffer.clear();
		}
		// deser
		User user = schema.newMessage();
		JsonIOUtil.mergeFrom(json, user, schema, numeric);
		long end = System.currentTimeMillis();
		System.out.println("5耗时:" + (end - start));
		System.out.println(user);
	}

}
