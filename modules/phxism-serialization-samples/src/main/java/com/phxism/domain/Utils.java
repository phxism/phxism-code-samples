package com.phxism.domain;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public abstract class Utils {

	public static byte[] serialize(Serializable object) {
		if (object == null) {
			return null;
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream(1024);
		try {
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(object);
			oos.flush();
		} catch (IOException ex) {
			throw new IllegalArgumentException(
					"Failed to serialize object of type: " + object.getClass(),
					ex);
		}
		return baos.toByteArray();
	}

	public static <T> T deserialize(byte[] bytes) {
		if (bytes == null) {
			return null;
		}
		try {
			ObjectInputStream ois = new ObjectInputStream(
					new ByteArrayInputStream(bytes));
			@SuppressWarnings("unchecked")
			T t = (T) ois.readObject();
			return t;
		} catch (IOException ex) {
			throw new IllegalArgumentException("Failed to deserialize object",
					ex);
		} catch (ClassNotFoundException ex) {
			throw new IllegalStateException(
					"Failed to deserialize object type", ex);
		}
	}

}
